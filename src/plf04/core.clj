(ns plf04.core)

;Primer problema recursividad lineal
(defn string-e-1
  [s]
  (<= 1 (letfn [(f [x]
          (if (empty? x) 0  (+ (if (= (first x) \e) 1 0)  (f (rest x)))))]
    (f s)) 3))

;Pruebas mias
(string-e-1 "Ho")
(string-e-1 "Hell")
(string-e-1 "Heeeeee")
(string-e-1 "Haaaaaaall")
(string-e-1 "eahee")
(string-e-1 "hahaheehehe")

;Todas las pruebas
(string-e-1 "Ho")
(string-e-1 "Hell")
(string-e-1 "Heeeeee")
(string-e-1 "Haaaaaaall")
(string-e-1 "eahee")
(string-e-1 "hahaheehehe")
(string-e-1 "Hello")
(string-e-1 "Heelle")
(string-e-1 "Heelele")
(string-e-1 "Hill")
(string-e-1 "e")
(string-e-1 "")

;Pruebas de codingbat
(string-e-1 "Hello")
(string-e-1 "Heelle")
(string-e-1 "Heelele")
(string-e-1 "Hill")
(string-e-1 "e")
(string-e-1 "")

;Primer problema recursividad  en cola
(defn string-e-2
  [a]
  (letfn [(f [s acc]
            (if (zero? (count s))
              (and (>= acc 1) (<= acc 3))
              (if (= (first s) \e)
                (f (rest s) (inc acc))
                (f (rest s) acc))))]
    (f a 0)))

;Pruebas del profe

(let [xs ["Hello" "Heelle" "Heelele" "Hill" "e" " "]]
  (map (fn [x] {:x x
                :st1 (string-e-1 x)
                :st2 (string-e-2 x)})
       xs))

(let [xs ["Hello" (list \H \e \l \l \o) (vector \H \e \l \l \o) (hash-set \H \e \l \l \o)]]
  (map (fn [x] {:x x
                :st1 (string-e-1 x)
                :st2 (string-e-2 x)})
       xs))

;Pruebas mias
(string-e-2 "Ho")
(string-e-2 "Hell")
(string-e-2 "Heeeeee")
(string-e-2 "Haaaaaaall")
(string-e-2 "eahee")
(string-e-2 "hahaheehehe")

;Todas las pruebas
(string-e-2 "Ho")
(string-e-2 "Hell")
(string-e-2 "Heeeeee")
(string-e-2 "Haaaaaaall")
(string-e-2 "eahee")
(string-e-2 "hahaheehehe")
(string-e-2 "Hello")
(string-e-2 "Heelle")
(string-e-2 "Heelele")
(string-e-2 "Hill")
(string-e-2 "e")
(string-e-2 "")

;Pruebas de codingbat
(string-e-2 "Hello")
(string-e-2 "Heelle")
(string-e-2 "Heelele")
(string-e-2 "Hill")
(string-e-2 "e")
(string-e-2 "")

;Segundo problema recursividad lineal

(defn string-times-1
  [s n]
  (letfn [(f [x y]
             (if (zero? y)
               ""
               (str x (f x (dec y)))))]
    (f s n)))

;pruebas mias
(string-times-1 "xx" 2)
(string-times-1 "asdas" 3)
(string-times-1 "h" 1)
(string-times-1 "GH" 0)
(string-times-1 "GLHF" 5)

;Todas las pruebas
(string-times-1 "xx" 2)
(string-times-1 "asdas" 3)
(string-times-1 "h" 1)
(string-times-1 "GH" 0)
(string-times-1 "GLHF" 5)
(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

;Pruebas codingbat
(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

;Segundo problema recursividad en cola

(defn string-times-2
  [s n]
  (letfn [(f [x y acc]
            (if (zero? y)
              acc
              (f x (dec y) (str s acc))))]
    (f s n "")))

;pruebas mias
(string-times-2 "xx" 2)
(string-times-2 "asdas" 3)
(string-times-2 "h" 1)
(string-times-2 "GH" 0)
(string-times-2 "GLHF" 5)

;Todas las pruebas
(string-times-2 "xx" 2)
(string-times-2 "asdas" 3)
(string-times-2 "h" 1)
(string-times-2 "GH" 0)
(string-times-2 "GLHF" 5)
(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)

;Pruebas codingbat
(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)

;Tercer problema recursividad lineal

(defn front-times-1
  [s n]
  (letfn [(g [x]
             (first x))
          (h [x]
             (rest x))
           (f [x y]
            (if (or (zero? y) (empty? x))
              ""
              (if (<= (count x) 3)
                (str x (f x (dec y)))
                (str (g x) (g (h x)) (g (h (h x))) (f x (dec y))))))
            ]
    (f s n)))

;Pruebas mias
(front-times-1 "Choc" 4)
(front-times-1 "Chlate" 5)
(front-times-1 "Abcdfe" 3)
(front-times-1 "Abasdasdasdas" 2)
(front-times-1 "A" 8)
(front-times-1 "" 2)
(front-times-1 "Acdb" 6)

;Todas las pruebas
(front-times-1 "Choc" 4)
(front-times-1 "Chlate" 5)
(front-times-1 "Abcdfe" 3)
(front-times-1 "Abasdasdasdas" 2)
(front-times-1 "A" 8)
(front-times-1 "" 2)
(front-times-1 "Acdb" 6)
(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

;Pruebas codingbat
(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

;Tercer problema recursividad en cola
(defn front-times-2
  [x y]
  (letfn [(i [x y z]
            (if (<= (count x) 3)
              (str x (f x (dec y) z))
              (str (first x) (first (rest x)) (first (rest (rest x))) (f x (dec y) z))))
          (f [s y acc]
            (if (or (zero? y) (empty? s))
              acc
              (i s y acc)))]
    (f x y "")))

;Pruebas mias
(front-times-2 "Choc" 4)
(front-times-2 "Chlate" 5)
(front-times-2 "Abcdfe" 3)
(front-times-2 "Abasdasdasdas" 2)
(front-times-2 "A" 8)
(front-times-2 "" 2)
(front-times-2 "Acdb" 6)

;Todas las pruebas
(front-times-2 "Choc" 4)
(front-times-2 "Chlate" 5)
(front-times-2 "Abcdfe" 3)
(front-times-2 "Abasdasdasdas" 2)
(front-times-2 "A" 8)
(front-times-2 "" 2)
(front-times-2 "Acdb" 6)
(front-times-2 "Chocolate" 2)
(front-times-2 "Chocolate" 3)
(front-times-2 "Abc" 3)
(front-times-2 "Ab" 4)
(front-times-2 "A" 4)
(front-times-2 "" 4)
(front-times-2 "Abc" 0)

;Pruebas codingbat
(front-times-2 "Chocolate" 2)
(front-times-2 "Chocolate" 3)
(front-times-2 "Abc" 3)
(front-times-2 "Ab" 4)
(front-times-2 "A" 4)
(front-times-2 "" 4)
(front-times-2 "Abc" 0)


;Cuarto problema recursividad lineal
(defn count-xx-1
  [s]
  (letfn [(g [x] 
             (if (and (= \x (first x)) (= \x (first (rest x))))
               1 
               0))
          (f [x]
            (if (empty? x)
              0
              (+ (g x)
                 (f (rest x)))))]
    (f s)))

;Pruebas mias
(count-xx-1 "abx")
(count-xx-1 "a12312x")
(count-xx-1 "axxbcxx")
(count-xx-1 "abcx2")
(count-xx-1 "abcx13xx1")

;Todas las pruebas
(count-xx-1 "abx")
(count-xx-1 "a12312x")
(count-xx-1 "axxbcxx")
(count-xx-1 "abcx2")
(count-xx-1 "abcx13xx1")
(count-xx-1 "abcxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "Hexxo thxxe")
(count-xx-1 "")
(count-xx-1 "Kittens")
(count-xx-1 "Kittensxxx")

;Pruebas codingbat
(count-xx-1 "abcxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "Hexxo thxxe")
(count-xx-1 "")
(count-xx-1 "Kittens")
(count-xx-1 "Kittensxxx")

;Cuarto problema recursividad en cola
(defn count-xx-2
  [a]
  (letfn [(g [x y]
            (if (and (= \x (first x)) (= \x (first (rest x))))
              (f (rest x) (inc y))
              (f (rest x) y)))
          (f [s acc]
            (if (empty? s)
              acc
              (g s acc)))]
    (f a 0)))

;Pruebas mias
(count-xx-2 "abx")
(count-xx-2 "a12312x")
(count-xx-2 "axxbcxx")
(count-xx-2 "abcx2")
(count-xx-2 "abcx13xx1")

;Todas las pruebas
(count-xx-2 "abx")
(count-xx-2 "a12312x")
(count-xx-2 "axxbcxx")
(count-xx-2 "abcx2")
(count-xx-2 "abcx13xx1")
(count-xx-2 "abcxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "Hexxo thxxe")
(count-xx-2 "")
(count-xx-2 "Kittens")
(count-xx-2 "Kittensxxx")

;Pruebas codingbat
(count-xx-2 "abcxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "Hexxo thxxe")
(count-xx-2 "")
(count-xx-2 "Kittens")
(count-xx-2 "Kittensxxx")

;Quinto problema recursividad lineal
(defn string-splosion-1
  [s]
  (letfn [(f [x]
            (if (== 0 (count x))
              x
              (apply str (f (subs x 0 (- (count x) 1))) x)))]
    (f s)))

;Pruebas de codinbat
(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

;Quinto problema recursividad en cola
(defn string-splosion-2
  [a]
  (letfn [(f [s acc]
            (if (== 0 (count s))
              acc
              (f (subs s 0 (- (count s) 1)) (str s acc))))]
    (f a "")))



;Pruebas mias
(string-splosion-2 "Cod")
(string-splosion-2 "KoreWa")
(string-splosion-2 "Code")
(string-splosion-2 "kora")
(string-splosion-2 "trex")

;Todas las pruebas
(string-splosion-2 "Cod")
(string-splosion-2 "KoreWa")
(string-splosion-2 "Code")
(string-splosion-2 "kora")
(string-splosion-2 "trex")
(string-splosion-2 "Code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")

;Pruebas de codingbat
(string-splosion-2 "Code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")

;Sexto problema recursividad lineal

(defn array-123-1
  [xs]
  (letfn [(f [ys]
             (if (or (empty? ys) (< (count ys) 3))
               false
               (if (and (= 1 (first ys))
                        (= 2 (first (rest ys)))
                        (= 3 (first (rest (rest ys)))))
                 true
                 (f (rest ys)))))]
    (f xs)))

;Pruebas recientes
(array-123-1 [1 2 2 3 4])
(array-123-1 [1 2 3 2 3 1 4])
(array-123-1 [1 1 1 2 2 3 3])
(array-123-1 [1 23 1 2 4 1 2])
(array-123-1 [1 2])

;Todas- las pruebas
(array-123-1 [1 2 2 3 4])
(array-123-1 [1 2 3 2 3 1 4])
(array-123-1 [1 1 1 2 2 3 3])
(array-123-1 [1 23 1 2 4 1 2])
(array-123-1 [1 2])
(array-123-1 [1 2 4 2 2 1])
(array-123-1 [1000 20000 30000 1 2 50000 2 100000])
(array-123-1 [1000 20000 30000 1 2 3 50000 2 100000])
(array-123-1 [123 1 2 3 4 5 6 7 8 9 10])

;Prueb-as de codingbat
(array-123-1 [1 1 2 3 1])
(array-123-1 [1 1 2 4 1])
(array-123-1 [1 1 2 1 2 3])
(array-123-1 [1 1 2 1 2 1])
(array-123-1 [1 2 3 1 2 3])
(array-123-1 [1 2 3])
(array-123-1 [1 1 1])
(array-123-1 [1 2])
(array-123-1 [1])

;Sexto problema recursividad en cola
          
(defn array-123-2
  [xs]
  (letfn [(g [ys x]
            (if (and (= 1 (first ys))
                     (= 2 (first (rest ys)))
                     (= 3 (first (rest (rest ys)))))
              x
              (f (rest ys) x)))
          (f [ys acc]
            (if (or (empty? ys) (< (count ys) 3))
              (false? acc)
              (g ys acc)))]
    (f xs true)))

;Pruebas recientes
(array-123-2 [1 2 2 3 4])
(array-123-2 [1 2 3 2 3 1 4])
(array-123-2 [1 1 1 2 2 3 3])
(array-123-2 [1 23 1 2 4 1 2])
(array-123-2 [1 2])

;Todas las pruebas
(array-123-2 [1 2 2 3 4])
(array-123-2 [1 2 3 2 3 1 4])
(array-123-2 [1 1 1 2 2 3 3])
(array-123-2 [1 23 1 2 4 1 2])
(array-123-2 [1 2])
(array-123-2 [1 2 4 2 2 1])
(array-123-2 [1000 20000 30000 1 2 50000 2 100000])
(array-123-2 [1000 20000 30000 1 2 3 50000 2 100000])
(array-123-2 [123 1 2 3 4 5 6 7 8 9 10])

;Pruebas de codingbat
(array-123-2 [1 1 2 3 1])
(array-123-2 [1 1 2 4 1])
(array-123-2 [1 1 2 1 2 3])
(array-123-2 [1 1 2 1 2 1])
(array-123-2 [1 2 3 1 2 3])
(array-123-2 [1 2 3])
(array-123-2 [1 1 1])
(array-123-2 [1 2])
(array-123-2 [1])
(array-123-2 [])


;Septimo problema recursividad lineal

(defn string-x-1
  [s]
  (letfn [(g [x] 
             (if (= (first x) \x)
               ""
               (str (first x))))
          (f [x]
            (if (or (empty? x) (= (count x) 1))
               ""
             (if (= (count (rest x)) 1)
               (str (first (rest x)))
               (str (g (rest x)) (f (rest x))))))]
    (str (first s)(f s))))

;Pruebas mias
(string-x-1 "x1231x3asdasdx")
(string-x-1 "xaxaxaxaxa")
(string-x-1 "aweasxaxaxa")
(string-x-1 "x123x")
(string-x-1 "1231241234")

;Todas las pruebas
(string-x-1 "x1231x3asdasdx")
(string-x-1 "xaxaxaxaxa")
(string-x-1 "aweasxaxaxa")
(string-x-1 "x123x")
(string-x-1 "1231241234")
(string-x-1 "xxHxix")
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")
(string-x-1 "x123x")
(string-x-1 "x1234")
(string-x-1 "xix")
(string-x-1 "32xx")
(string-x-1 "xab123123dx")

;Pruebas de codingbat
(string-x-1 "xxHxix")
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")

;Septimo problema recursividad en cola

(defn string-x-2
  [a]
  (letfn [(g [x y]
            (if (and (= \x (first x)) (> (count x) 1))
              (f (rest x) (inc y))
              (str (first x) (f (rest x) (inc y)))))
          (h [x y]
            (if (= y 0)
              (str (first x) (f (rest x) (inc y)))
              (g x y)))
          (f [s acc]
            (if (empty? s)
              ""
              (h s acc)))]
    (f a 0)))

;Pruebas mias
(string-x-2 "x1231x3asdasdx")
(string-x-2 "xaxaxaxaxa")
(string-x-2 "aweasxaxaxa")
(string-x-2 "x123x")
(string-x-2 "1231241234")

;Todas las pruebas
(string-x-2 "x1231x3asdasdx")
(string-x-2 "xaxaxaxaxa")
(string-x-2 "aweasxaxaxa")
(string-x-2 "x123x")
(string-x-2 "1231241234")
(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")
(string-x-2 "x123x")
(string-x-2 "x1234")
(string-x-2 "xix")
(string-x-2 "32xx")
(string-x-2 "xab123123dx")

;Pruebas de codingbat
(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")

;Octavo problema recursividad lineal

(defn alt-pairs-1
  [s]
  (letfn [(f [x]
            (if (empty? x)
              "" (if (even? (count x)) 
                   (if (< 2 (count x)) (str (subs x 0 2) (f (subs x 4)))
                       (subs x 0 2))
                     (if (< 3 (count x)) (str (subs x 0 2) (f (subs x 4)))
                         (if (< 1 (count x)) 
                           (subs x 0 2) 
                           (subs x 0 1))))))]
    (f s)))

;Pruebas mias
(alt-pairs-1 "chocccccc")
(alt-pairs-1 "cho")
(alt-pairs-1 "asdasd")
(alt-pairs-1 "1")
(alt-pairs-1 "czxxc")

;Todas las pruebas
(alt-pairs-1 "chocccccc")
(alt-pairs-1 "cho")
(alt-pairs-1 "asdasd")
(alt-pairs-1 "1")
(alt-pairs-1 "czxxc")
(alt-pairs-1 "kitten")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "Chocolate")
(alt-pairs-1 "yak")
(alt-pairs-1 "ya")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")

;Pruebas codingbat
(alt-pairs-1 "kitten")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "Chocolate")
(alt-pairs-1 "yak")
(alt-pairs-1 "ya")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")

;Octavo problema recursividad en cola

(defn alt-pairs-2
  [s]
  (letfn [(f [x i]
            (if (and (empty? x) (>= i 0))
                 "" 
              (if (even? (count x)) 
                (if (< 2 (count x)) 
                  (str (subs x 0 2) (f (subs x 4) (inc i)))
                    (subs x 0 2))
                (if (< 3 (count x)) (str (subs x 0 2) (f (subs x 4) (inc i)))
                    (if (< 1 (count x)) (subs x 0 2) (subs x 0 1))))))]
    (f s 0)))

;Pruebas mias
(alt-pairs-2 "chocccccc")
(alt-pairs-2 "cho")
(alt-pairs-2 "asdasd")
(alt-pairs-2 "1")
(alt-pairs-2 "czxxc")

;Todas las pruebas
(alt-pairs-2 "chocccccc")
(alt-pairs-2 "cho")
(alt-pairs-2 "asdasd")
(alt-pairs-2 "1")
(alt-pairs-2 "czxxc")
(alt-pairs-2 "kitten")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "Chocolate")
(alt-pairs-2 "yak")
(alt-pairs-2 "ya")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")

;Pruebas codingbat
(alt-pairs-2 "kitten")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "Chocolate")
(alt-pairs-2 "yak")
(alt-pairs-2 "ya")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")

;Noveno problema recursividad lineal

(defn string-yak-1
  [s]
  (letfn [(g [x y]
             (and (= (first x) \y) (= (first (rest y)) \k)))
          (f [x]
            (if (empty? x)
              ""
              (if (g x (rest x))
                (f (rest (rest (rest x))))
                (str (first x) (f (rest x))))))]
    (f s)))

;pruebas mias
(string-yak-1 "pakpasdkyakp")
(string-yak-1 "yxkp")
(string-yak-1 "y1kp")
(string-yak-1 "ky1kp")
(string-yak-1 "yayayayayayakyxkp")

;todas las pruebas
(string-yak-1 "pakpasdkyakp")
(string-yak-1 "yxkp")
(string-yak-1 "y1kp")
(string-yak-1 "ky1kp")
(string-yak-1 "yayayayayayakyxkp")
(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")
(string-yak-1 "y1p")
(string-yak-1 "kyxxxxkp")
(string-yak-1 "yayakyakyayayakyxkp")
(string-yak-1 "")
(string-yak-1 "kyx")
(string-yak-1 "asdask")

;pruebas de codingbat
(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")

;Noveno problema recursividad en cola

(defn string-yak-2
  [a]
  (letfn [(g [x y]
            (and (= \y x) (= \k y)))
          (h [x y]
            (if (g (first x) (first (rest (rest x))))
              (f (rest (rest (rest x))) y)
              (f (rest x) (str y (first x)))))
          (f [s acc]
            (if (empty? s)
              acc
              (h s acc)))]
    (f a "")))

;pruebas mias
(string-yak-2 "pakpasdkyakp")
(string-yak-2 "yxkp")
(string-yak-2 "y1kp")
(string-yak-2 "ky1kp")
(string-yak-2 "yayayayayayakyxkp")

;todas las pruebas
(string-yak-2 "pakpasdkyakp")
(string-yak-2 "yxkp")
(string-yak-2 "y1kp")
(string-yak-2 "ky1kp")
(string-yak-2 "yayayayayayakyxkp")
(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")
(string-yak-2 "y1p")
(string-yak-2 "kyxxxxkp")
(string-yak-2 "yayakyakyayayakyxkp")
(string-yak-2 "")
(string-yak-2 "kyx")
(string-yak-2 "asdask")

;pruebas de codingbat
(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")

;Decimo problema recursividad lineal

(defn has-271-1
  [xs]
  (letfn [(g [x]
             (and (= (+ 5 (first x)) (first (rest x)))
                  (>= 2 (h x))))
          (h [x]
             (if (neg? (- (first (rest (rest x))) (- (first x) 1)))
               (* -1 (- (first (rest (rest x))) (- (first x) 1)))
               (- (first (rest (rest x))) (- (first x) 1))))
           (f [x] (if (empty? x)
                    false
                    (if (g x)
                    true
                    (f (rest x)))))]
    (f xs)))

;Pruebas mias
(has-271-1 [1 2])
(has-271-1 [7 1])
(has-271-1 [1 2 7 5 1 2 3 4 5 6 7 8])
(has-271-1 [1 2 7 2 8 4 5 6 7 8])
(has-271-1 [1 2 7 5 1 2 1 7 8])

;Todas las pruebas
(has-271-1 [1 2])
(has-271-1 [7 1])
(has-271-1 [1 2 7 5 1 2 3 4 5 6 7 8])
(has-271-1 [1 2 7 2 8 4 5 6 7 8])
(has-271-1 [1 2 7 5 1 2 1 7 8])
(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2, 4 9 3])
(has-271-1 [2 7 5, 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])

;Pruebas codingbat
(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2 4 9 3])
(has-271-1 [2 7 5, 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])

;Decimo problema recursividad de cola

(defn has-271-2
  [xs]
  (letfn [(g [x]
            (and (= (+ 5 (first x)) (first (rest x)))
                 (>= 2 (h x))))
          (h [x]
            (if (neg? (- (first (rest (rest x))) (- (first x) 1)))
              (* -1 (- (first (rest (rest x))) (- (first x) 1)))
              (- (first (rest (rest x))) (- (first x) 1))))
          (f [x acc] (if (empty? x)
                       (false? acc)
                       (if (g x)
                         acc
                         (f (rest x) acc))))]
    (f xs true)))

;Pruebas mias
(has-271-2 [1 2])
(has-271-2 [7 1])
(has-271-2 [1 2 7 5 1 2 3 4 5 6 7 8])
(has-271-2 [1 2 7 2 8 4 5 6 7 8])
(has-271-2 [1 2 7 5 1 2 1 7 8])

;Todas las pruebas
(has-271-2 [1 2])
(has-271-2 [7 1])
(has-271-2 [1 2 7 5 1 2 3 4 5 6 7 8])
(has-271-2 [1 2 7 2 8 4 5 6 7 8])
(has-271-2 [1 2 7 5 1 2 1 7 8])
(has-271-2 [1 2 7 1])
(has-271-2 [1 2 8 1])
(has-271-2 [2 7 1])
(has-271-2 [3 8 2])
(has-271-2 [2 7 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4 5 3 8 0])
(has-271-2 [2 7 5 10 4])
(has-271-2 [2 7 -2, 4 9 3])
(has-271-2 [2 7 5, 10 1])
(has-271-2 [2 7 -2 4 10 2])
(has-271-2 [1 1 4 9 0])
(has-271-2 [1 1 4 9 4 9 2])

;Pruebas codingbat
(has-271-2 [1 2 7 1])
(has-271-2 [1 2 8 1])
(has-271-2 [2 7 1])
(has-271-2 [3 8 2])
(has-271-2 [2 7 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4 5 3 8 0])
(has-271-2 [2 7 5 10 4])
(has-271-2 [2 7 -2, 4 9 3])
(has-271-2 [2 7 5, 10 1])
(has-271-2 [2 7 -2 4 10 2])
(has-271-2 [1 1 4 9 0])
(has-271-2 [1 1 4 9 4 9 2])